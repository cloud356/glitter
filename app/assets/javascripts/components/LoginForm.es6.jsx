class LoginForm extends React.Component{
    constructor(props){
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {header: 'Login', username: '', password: ''};
    }

    handleSubmit(event) {
        let user = {
            username: this.state.username,
            password: this.state.password
        };
        $.post('/login', JSON.stringify(user), (data) => {
            if(data.status === "OK"){
                this.setState({ header: 'Shantay you stay' });
            }else{
                console.log(data);
                this.setState({ header: 'No good ' + JSON.stringify(data) });
            }
        });
        event.preventDefault();
        setTimeout(() => {
            location.reload();
        }, 800);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({ [name]: value });
    }

    render() {
        return (
            <form className="form-horizontal" onSubmit={this.handleSubmit}>
                <h4>{ this.state.header }</h4>
                <div className="form-group">
                    <label htmlFor="inputusernamelogin" className="col-sm-2 control-label">Username</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control"
                               id="inputusernamelogin" placeholder="Username" name="username"
                               value={this.state.username} onChange={this.handleChange} />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="inputpasswordlogin" className="col-sm-2 control-label">Password</label>
                    <div className="col-sm-10">
                        <input type="password" className="form-control"
                               id="inputpasswordlogin" placeholder="Password" name="password"
                               value={this.state.password} onChange={this.handleChange} />
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-sm-offset-2 col-sm-10">
                        <input type="submit" className="btn btn-default"/>
                    </div>
                </div>
            </form>
        );
    }
}