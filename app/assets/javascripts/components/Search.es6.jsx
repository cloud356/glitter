class LogoutBtn extends React.Component {
    constructor(props){
        super(props)
    }

    handleClick(event){

    }

    render() {
        return (
<div className="container">
    <div className="row">
        <div className="col-md-12">
            <div className="input-group" id="adv-search">
                <input type="text" className="form-control" placeholder="Search for snippets" />
                <div className="input-group-btn">
                    <div className="btn-group" role="group">
                        <div className="dropdown dropdown-lg">
                            <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span className="caret"></span></button>
                            <div className="dropdown-menu dropdown-menu-right" role="menu">
                                <form className="form-horizontal" role="form">
                                  <div className="form-group">
                                    <label for="filter">Filter by</label>
                                    <select className="form-control">
                                        <option value="0" selected>All Sparkles</option>
                                        <option value="1">Following</option>
                                        <option value="2">Most Likes</option>
                                        <option value="3">Most Replies</option>
                                        <option value="4">...</option>
                                    </select>
                                  </div>
                                  <div className="form-group">
                                    <label for="contain">Username</label>
                                    <input className="form-control" type="text" />
                                  </div>
                                  <div className="form-group">
                                    <label for="contain">Contains the words</label>
                                    <input className="form-control" type="text" />
                                  </div>
                                  <button type="submit" className="btn btn-primary"><span className="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                </form>
                            </div>
                        </div>
                        <button type="button" className="btn btn-primary"><span className="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
        );
    }
}
