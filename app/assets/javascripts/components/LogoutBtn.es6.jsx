class LogoutBtn extends React.Component {
    constructor(props){
        super(props)
    }

    handleClick(event){
        $.post('/logout', (data) => {
            if(data.status === "OK"){
                location.reload();
            }
        });
        event.preventDefault();
    }

    render() {
        return (
            <a onClick={this.handleClick()}>Logout</a>
        );
    }
}
