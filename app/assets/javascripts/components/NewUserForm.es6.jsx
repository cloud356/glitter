class NewUserForm extends React.Component {

    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {header: 'Sign Up', username: '', email: '', password: ''};
    }

    handleSubmit(event) {
        let user = {
            username: this.state.username,
            email: this.state.email,
            password: this.state.password
        };
        $.post('/adduser', JSON.stringify(user), (data) => {
            if(data.status === "OK"){
                this.setState({ header: 'Welcome hennyyyy key: ' + data.key  });
            }else{
                this.setState({ header: 'No good ' + JSON.stringify(data) });
            }
        });
        event.preventDefault();
    }

    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({ [name]: value });
    }

    render() {
        return (
            <form className="form-horizontal" onSubmit={this.handleSubmit}>
                <h4>{ this.state.header }</h4>
                <div className="form-group">
                    <label htmlFor="inputusername" className="col-sm-2 control-label">Username</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control"
                               id="inputusername" placeholder="Username" name="username"
                               value={this.state.username} onChange={this.handleChange} />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="inputemail" className="col-sm-2 control-label">Email</label>
                    <div className="col-sm-10">
                        <input type="email" className="form-control"
                               id="inputemail" placeholder="Email" name="email"
                               value={this.state.email} onChange={this.handleChange} />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="inputpassword" className="col-sm-2 control-label">Password</label>
                    <div className="col-sm-10">
                        <input type="password" className="form-control"
                               id="inputpassword" placeholder="Password" name="password"
                               value={this.state.password} onChange={this.handleChange} />
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-sm-offset-2 col-sm-10">
                        <input type="submit" className="btn btn-default"/>
                    </div>
                </div>
            </form>
        );
    }
}
