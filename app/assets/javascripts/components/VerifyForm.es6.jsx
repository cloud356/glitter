class VerifyForm extends React.Component{
    constructor(props){
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {header: 'Verify', email: '', key: ''};
    }

    handleSubmit(event) {
        let user = {
            email: this.state.email,
            key: this.state.key
        };
        $.post('/verify', JSON.stringify(user), (data) => {
            if(data.status === "OK"){
                this.setState({ header: 'Condragulations you are the winner of this week\'s challenge' });
            }else{
                this.setState({ header: 'No good ' + JSON.stringify(data) });
            }
        });
        event.preventDefault();
    }

    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({ [name]: value });
    }

    render() {
        return (
            <form className="form-horizontal" onSubmit={this.handleSubmit}>
                <h4>{ this.state.header }</h4>
                <div className="form-group">
                    <label htmlFor="verifyemail" className="col-sm-2 control-label">Email</label>
                    <div className="col-sm-10">
                        <input type="text" className="disable form-control"
                               id="verifyemail" placeholder="Email" name="email"
                               value={this.state.email} onChange={this.handleChange} />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="verifykey" className="col-sm-2 control-label">Key</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control"
                               id="verifykey" placeholder="Key" name="key"
                               value={this.state.key} onChange={this.handleChange} />
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-sm-offset-2 col-sm-10">
                        <input type="submit" className="btn btn-default"/>
                    </div>
                </div>
            </form>
        );
    }
}