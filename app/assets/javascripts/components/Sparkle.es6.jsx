class Sparkle extends React.Component{
    constructor(props){
        super(props);
        this.props = props;
        this.state = {content: props.content, username: props.username};
    }
    render(){
        return (
            <div className="sparkle">
                <p><strong>{this.props.username + ": "}</strong> <span className="rainbow">{this.props.content}</span></p>
            </div>
        );
    }
}
