class NewSparkleForm extends React.Component {
    constructor(props){
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {header: 'New Sparkle', content: ''};
    }

    handleSubmit(event) {
        let spark = {
            content: this.state.content
        };
        $.post('/additem', JSON.stringify(spark), (data) => {
            if(data.status === "OK"){
                this.setState({ header: 'Loves it...' });
            }else{
                this.setState({ header: 'No good ' + JSON.stringify(data) });
            }
        });
        event.preventDefault();
        setTimeout(() => {
            location.reload();
        }, 800);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({ [name]: value });
    }

    render() {
        return (
            <form className="form-horizontal" onSubmit={this.handleSubmit}>
                <h4>{ this.state.header }</h4>
                <div className="form-group">
                    <label htmlFor="sparklecontent" className="col-sm-2 control-label">Glamour:</label>
                    <div className="col-sm-10">
                        <textarea className="disable form-control"
                               id="sparklecontent" placeholder="Shine bright" name="content"
                               value={this.props.email} onChange={this.handleChange} />
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-sm-offset-2 col-sm-10">
                        <input type="submit" className="btn btn-default"/>
                    </div>
                </div>
            </form>
        );
    }
}