class ApplicationMailer < ActionMailer::Base
  default from: 'theshinysquadthatglitters@gmail.com'
  layout 'mailer'
end
