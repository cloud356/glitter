class UserMailer < ApplicationMailer
  default :from => "theshinysquadthatglitters@gmail.com"

  def email_verify_token_req(user, domain)
    @user = user
    @domain = domain
    mail(:to => "#{@user.username} <#{@user.email}>", :subject => "Get Ur Glitter Gurl")
  end
end
