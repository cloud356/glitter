class UsersController < ApplicationController
  before_action :current_user, only: [:update, :destroy, :follow]
  before_action :json_check
  before_action :follow_check, only: [:followers, :following]

  def create
    @user = User.new(username: @json[:username], password: @json[:password], email: @json[:email])
    if @user.save
      # UserMailer.email_verify_token_req(@user, request.domain).deliver
      send_status :OK, additional: {key: @user.token}
    else
      send_status :error, reason: @user.errors.full_messages.join(' ')
    end
  end

  def retrieve
    @usertoget = User.find_by username: params[:username]
    if @usertoget
      send_status :OK, additional: {user: @usertoget.to_json}
    else
      send_status :error, reason: 'User with that name does not exist'
    end
  end

  def followers
    @usertoget = User.find_by username: params[:username]
    if @usertoget
      followers = User.find(@usertoget.followers[0, @limit]).map{|x| x[:username]}
      send_status :OK, additional: {users: followers}
    else
      send_status :error, reason: 'User with that name does not exist'
    end
  end

  def following
    if (@usertoget = User.find_by username: params[:username])
      following = User.find(@usertoget.following[0, @limit]).map{|x| x[:username]}
      send_status :OK, additional: {users: following}
    else
      send_status :error, reason: 'User with that name does not exist'
    end
  end

  def follow
    #Rails.logger.warn "[FOLLOW BEGIN] -- #{@user.username} is #{ @json[:follow] ? 'following' : 'unfollowing' } #{@json[:username]}"
    if @user.username != @json[:username]
      if (tofollow = User.find_by(username: @json[:username]))
        if @json[:follow]
          @user.following << tofollow.id unless @user.following.include? tofollow.id
          tofollow.followers << @user.id unless tofollow.followers.include? @user.id
        else
          #Rails.logger.warn "[FOLLOW DELETE BEFORE] -- #{@user.following}"
          @user.following.delete(tofollow.id)
          #Rails.logger.warn "[FOLLOW DELETE AFTER] -- #{@user.following}"
        end
      end
      if @user.save and tofollow.save
        send_status :OK
        #Rails.logger.warn "[FOLLOW END] -- OK"
      else
        send_status :error, reason: 'Could not save to db'
        #Rails.logger.warn "[FOLLOW END] -- COULD NOT SAVE TO DB"
      end
    else
      send_status :error, reason: 'You cannot follow yourself'
      #Rails.logger.warn "[FOLLOW END] -- YOU CANT FOLLOW YOURSELF"
    end
  end

  def update
    send_status :error, reason: 'Not yet implemented'
  end

  def destroy
    send_status :error, reason: 'Not yet implemented'
  end

  def verify
    begin
      @user = User.find_by email: @json[:email]
      result = @user.confirm_user(@json[:key])
      if result[:worked?]
        send_status :OK
      else
        send_status :error, reason: result[:msg]
      end
    rescue Mongoid::Errors::DocumentNotFound => e
      send_status :error, reason: e.message
    end
  end

  private

  def follow_check
    if params[:limit]
      if params[:limit] < 200
        @limit = params[:limit]
      else
        @limit = 200
      end
    else
      @limit = 50
    end
  end
end
