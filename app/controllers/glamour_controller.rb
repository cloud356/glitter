class GlamourController < ApplicationController
  def create
    if params['content']
      g = Glamour.new
      g.content_type = params['content'].content_type
      g.ext = '.' + params['content'].original_filename.split('.').last
      g.pic = BSON::Binary.new(params['content'].tempfile.read, :generic)
      if g.save
        send_status :OK, additional: {id: g.id.to_s}
      else
        send_status :error, reason: 'Could not save Glamour'
      end
    else
      send_status :error, reason: 'Empty'
    end
  end

  def retrieve
    begin
      g = Glamour.find params[:id]
      send_data g.pic.data, type: g.content_type
    rescue Mongoid::Errors::DocumentNotFound => e
      send_status :error, reason: 'none by that name'
    end
  end

  def delete
    g = Glamour.find params[:id]
    g.remove_pic!
    g.save
  end

end
