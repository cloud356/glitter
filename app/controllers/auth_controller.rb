class AuthController < ApplicationController
  before_action :json_check

  def create
    begin
      @user = User.find_by username: @json[:username]
      if @user.active
        if @user.authenticate(@json[:password])
          # session[:user_id] = @user.id
          cookies[:session] = @user.id.to_s
          send_status :OK
        else
          send_status :error, reason: @user.errors.full_messages.join(" ")
        end
      else
        send_status :error, reason: "User is still inactive"
      end
    rescue Mongoid::Errors::DocumentNotFound => e
      send_status :error, reason: e.message
    end
  end

  def destroy
    cookies[:session] = nil
    # session[:user_id] = nil
    if request.method_symbol == :get
      redirect_to '/'
    else
      send_status :OK
    end
  end
end
