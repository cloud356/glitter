class SparkleController < ApplicationController
  before_action :has_session, except: [:search]
  before_action :current_user, only: [:search]
  before_action :json_check, except: [:retrieve]
  before_action :search_defaults, only: [:search]

  def create
    spark = Sparkle.create(content: @json[:content], user_id: @user.id)
    if  @json[:parent] and (parent = Sparkle.find @json[:parent])
      spark.parent_id = parent.id
      parent.replies << spark.id
      parent.save!
    end
    if @json[:media] and (glamours = Glamour.find @json[:media])
      spark.glamours = glamours
      glamours.each {|glam| glam.sparkle = spark.id}
    end
    if spark.save!
      send_status :OK, additional: { id: spark.id.to_s }
    else
      send_status :error, reason: @user.errors.full_messages.join(' ')
    end
  end

  def retrieve
    begin
      spark = Sparkle.find(params[:id])
      item = {
        id: spark.id.to_s,
        username: spark.user.username,
        content: spark.content,
        timestamp: spark.created_at.to_time.to_i,
        likes: spark.liked_by_size,
        replies: spark.replies_size,
        attn: spark.attn
      }
      item[:parent] = spark.parent.id.to_s if spark.parent
      item[:media]  = spark.glamours.map {|glam| glam.id.to_s} if spark.glamours
      send_status :OK, additional: {item: item}
    rescue Mongoid::Errors::DocumentNotFound => e
      send_status :error, reason: 'Item Not Found'
    end
  end

  def update
    begin
      sparkle = Sparkle.find(params[:id])
      if @user.likes.include? sparkle.id
        @user.likes.delete(sparkle.id)
        sparkle.liked_by.delete(@user.id)
      else
        @user.likes << sparkle.id
        sparkle.liked_by << @user.id
      end
      @user.save!
      sparkle.save!
      send_status :OK, reason: 'That sparkle DNE'
    rescue Mongoid::Errors::DocumentNotFound => e
      send_status :error, reason: 'That sparkle DNE'
    end
  end

  def destroy
    if (spark = @user.sparkles.find params[:id])
        spark.glamours.each do |glam|
          glam.destroy
        end
      spark.destroy
      send_status :OK
    else
      send_status :error, reason: 'Nothing to delete'
    end
  end

  def search
    items = []
    sparks = Sparkle.where(:created_at.lte => Time.zone.at(@json[:timestamp]+2))
    sparks = sparks.where(user_id: @json[:user_id_looking_for]) if @json[:user_id_looking_for]
    sparks = sparks.where(:user_id.in =>  @user.following) if @user and @json[:following] and @user.following.any?

    sparks = sparks.where(parent: nil) unless @json[:replies]
    sparks = sparks.where(parent: @json[:parent]) if @json[:parent]

    sparks = sparks.order_by(created_at: :desc) if @json[:rank] == 'time'
    sparks = sparks.order_by(attn: :desc) if @json[:rank] == 'interest'

    sparks = sparks.search(@json[:q]) if @json[:q]
    sparks.limit(@json[:limit]).each do |spark|
      item = {
        id: spark.id.to_s,
        username: spark.user.username,
        content: spark.content,
        timestamp: spark.created_at.to_time.to_i,
        likes: spark.liked_by_size,
        replies: spark.replies_size,
        attn: spark.attn
      }
      item[:parent] = spark.parent.id.to_s if spark.parent
      item[:media]  = spark.glamours.map {|glam| glam.id.to_s} if spark.glamours
      items << item
    end
    send_status :OK, additional: { :items => items }
  end

  private
    def search_defaults
      unless @json.has_key?(:timestamp)
        @json[:timestamp] = Time.zone.now.to_i
      end
      unless @json.has_key?(:limit)
        @json[:limit] = 25
      end
      unless @json.has_key?(:following)
        @json[:following]
      end
      unless @json.has_key?(:rank)
        @json[:rank] = 'interest'
      end
      unless @json.has_key?(:replies)
        @json[:replies] = true
      end
      if @json.has_key?(:username)
        begin
          @json[:user_id_looking_for] = User.find_by(username: @json[:username]).id
        rescue Mongoid::Errors::DocumentNotFound => e
          send_status :error, reason: 'Invalid username' unless @json[:user_id_looking_for]
        end
      end
    end

end
