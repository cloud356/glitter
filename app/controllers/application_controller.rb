class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token

  helper_method :current_user

  def current_user
    if cookies[:session]
      begin
        @user = User.find(cookies[:session])
      rescue Mongoid::Errors::DocumentNotFound => e
        nil
      end
    else
      #@user ||= User.find(session[:user_id]) if session[:user_id]
      nil
    end
  end

  helper_method :has_session

  def has_session
    unless current_user
      send_status :error, reason: 'User session DNE'
    end
  end

  helper_method :json_check

  def json_check
    input = request.body.read
    unless json_parsable input
      input = params.to_json.to_s
    end
    @json = JSON.parse(input, symbolize_names: true)
    schema = "#{Rails.root.to_s}/public/assets/schemas/#{controller_name}/#{action_name}_schema.json"
    if File.exist? schema
      begin
        JSON::Validator.validate!(schema, @json, insert_defaults: true, clear_cache: Rails.env.development?)
        @json = @json.inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}  # Magic all strings to symbol fix
      rescue JSON::Schema::ValidationError => e
        send_status :error, reason: e.message
      end
    else
      Rails.logger.warn "Schema DNE for #{controller_name}/#{action_name} henny"
    end
  end

  helper_method :send_status

  def send_status(status, reason: '', additional: {})
    status = status.to_sym
    if status == :OK
      render json: {:status => status}.merge(additional)
    elsif status == :error
      render json: {:status => status, :error => reason}.merge(additional)
    else
      raise ArgumentError.new("Invalid status: #{status} passed")
    end
  end

  private
  def json_parsable string
    !!JSON.parse(string, symbolize_names: true) rescue !!nil
  end

end
