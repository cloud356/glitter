class Glamour
  include Mongoid::Document
  belongs_to :sparkle, optional: true

  field :pic, type: BSON::Binary

  field :ext, type: String
  field :content_type, type: String

end
