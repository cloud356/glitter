class Sparkle
  include Mongoid::Document
  include Mongoid::Timestamps::Created
  belongs_to :user, validate: true
  has_many :glamours

  belongs_to :parent, class_name: 'Sparkle', optional: true
  field :replies, type: Array, default: []


  field :content, type: String
  field :liked_by, type: Array, default: []

  field :liked_by_size, type: Integer, default: 0
  field :replies_size, type: Integer, default: 0
  field :attn, type: Integer, default: 0

  # searchkick

  # def search_data
  #   { content: self.content }
  # end

  before_save do
    self.liked_by_size = liked_by.size
    self.replies_size = replies.size
    self.attn = liked_by.size + replies.size
  end

  index({ content: 'text'})

  def self.search(q)
    Sparkle.where({ :$text => { :$search => q, :$language => 'en'}})
  end

  def to_json
    {
        content: self.content,
        username: User.find(self.user_id).username
    }
  end

  def before_destroy
    self.glamours.each do |glam|
      glam.pic.remove!
    end
  end
end
