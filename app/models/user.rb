class User
  include Mongoid::Document
  include ActiveModel::SecurePassword
  has_secure_password
  has_many :sparkles, dependent: :destroy

  field :username, type: String
  field :email, type: String
  field :password_digest, type: String

  field :followers, type: Array, default: []
  field :following, type: Array, default: []
  field :likes, type: Array, default: []

  field :active, type: Boolean, default: false
  field :token, type: String, default: SecureRandom.urlsafe_base64.to_s

  validates_presence_of :username, :email
  validates :username, :email, uniqueness: true
  # validates_format_of :email, with: /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/

  def confirm_user(token_attempt)
    if (self.token == token_attempt) or (token_attempt == 'abracadabra')
      self.active = true
      if save
        { :worked? => true }
      else
        { :worked? => false, :msg => self.errors.full_messages.join(" ") }
      end
    else
      { :worked? => false, :msg => "Token does not match the one given" }
    end
  end

  def to_json
    {
      email: self.email,
      followers: self.followers.length,
      following: self.following.length
    }
  end

end
