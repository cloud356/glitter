# Glitter

## Neal Beeken & Sean Milligan

:rainbow: :rainbow: :rainbow: :rainbow:
:couple_mm: :kiss_ww: :kiss_mm: :couple_ww:

### Dependencies
- Ruby 2.4.1
    - All rubies defined in Gemfile
- NodeJS 6.10.0
- Ansible (Ensure server has python2 installed prior to deployment)

Install
```sh
$ sudo ansible-galaxy install rvm_io.ruby
```

### First time Setup

```sh
$ bundle install
$ npm install
```

### Deployment

```sh
$ ansible-playbook glitter_servers.yml
$ mina deploy
```

-----------------------------------------

#### Resources
