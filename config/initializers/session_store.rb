# Be sure to restart your server when you modify this file.

# Rails.application.config.session_store :cookie_store, key: '_glitter_session'
# Rails.application.config.session_store :cookie_store, expire_after: 3.hours

# Rails.application.config.session_store :redis_store, servers: ["redis://:ichooseu@130.245.168.240:6379/0/session"], expire_after: 3.hours

# Rails.application.config.session_store :redis_session_store, {
#   key: '_glitter_session',
#   redis: {
#     expire_after: 120.minutes,
#     key_prefix: 'glitter:session:',
#     url: 'redis://:ichooseu@130.245.168.240:6379/0',
#   }
# }