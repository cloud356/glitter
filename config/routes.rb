Rails.application.routes.draw do

  root to: 'home#index'

  # Tweets or Sparkles
  get '/item/:id/like', to: 'sparkle#update'
  post '/item/:id/like', to: 'sparkle#update'
  get '/item/:id', to: 'sparkle#retrieve'
  delete '/item/:id', to: 'sparkle#destroy'
  post '/additem', to: 'sparkle#create'
  post '/search', to: 'sparkle#search'

  # Users
  post '/adduser', to: 'users#create'
  post '/verify', to: 'users#verify'

  get '/user/:username', to: 'users#retrieve'
  get '/user/:username/followers', to: 'users#followers'
  get '/user/:username/following', to: 'users#following'

  post '/follow', to: 'users#follow'

  # Authentication
  post '/login', to: 'auth#create'
  post '/logout', to: 'auth#destroy'

  # Logout from view
  get '/logout', to: 'auth#destroy'

  post '/addmedia', to: 'glamour#create'
  get '/media/:id', to: 'glamour#retrieve'

  # get '*path' => redirect('/404.html')
  # post '*path' => redirect('/404.html')
  # delete '*path' => redirect('/404.html')

end
