require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'action_cable/engine'
require 'sprockets/railtie'
require 'rails/test_unit/railtie'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Glitter
  class Application < Rails::Application
    config.middleware.use ActionDispatch::Session::CookieStore
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.browserify_rails.commandline_options =
        '-t [ babelify --presets [ react es2015 ] ]'
    unless Rails.env.production?
      # config.logger = LogStashLogger.new(type: :udp, host: 'lugia', port: 5228)
      # Make sure Browserify is triggered when asked to serve javascript spec files
      config.browserify_rails.paths << lambda { |p|
          p.start_with?(Rails.root.join('spec/javascripts').to_s)
      }
    end
  end
end
